# -*- coding: utf-8 -*-
{
    'name': 'Website Attach PDF',
    'version': '13.0.1.0.0',
    'author': 'HomebrewSoft',
    'website': 'https://homebrewsoft.dev/',
    'license': 'LGPL-3',
    'depends': [
        'website_sale',
    ],
    'data': [
        # security
        # data
        # reports
        # views
        'views/product_template.xml',
    ],
}
