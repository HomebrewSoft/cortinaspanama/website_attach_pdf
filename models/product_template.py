# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    website_pdf = fields.Binary(
        string='Website PDF',
    )
